import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() {
  runApp(const MaterialApp(
    home: XylophonePage(),
  ));
}

class XylophonePage extends StatelessWidget {
  const XylophonePage({Key? key}) : super(key: key);
  static AudioCache player = AudioCache();

  void playSound(int _id) {
    player.play('note$_id.wav');
  }

  Expanded displayXyloKey(Color color, int id) {
    return Expanded(
      child: TextButton(
        onPressed: () => playSound(id),
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(0.0),
        ),
        child: Container(
          color: color
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          displayXyloKey(Colors.red, 1),
          displayXyloKey(Colors.orange, 2),
          displayXyloKey(Colors.yellow, 3),
          displayXyloKey(Colors.green, 4),
          displayXyloKey(Colors.teal, 5),
          displayXyloKey(Colors.blue, 6),
          displayXyloKey(Colors.purple, 7),
        ],
      ),
    );
  }
}
